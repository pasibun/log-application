from datetime import time
from flask import Flask, render_template, request, jsonify
import mysql.connector

app = Flask(__name__)
mydb = mysql.connector.connect(
    host="0.0.0.0",
    port=0,
    user="****",
    password="****",
    database="**"
)
mycursor = mydb.cursor()

get_menu = 'select * from menu;'
get_storingen = 'select * from storingen;'


class Item:
    def __init__(self, tijd, error):
        self.time = tijd
        self.error = error


items = []
menu = ['Vormwalsen',
        'Verkeerd geplaatse spacer',
        'Seperator storing',
        'Grijper I',
        'Grijper II',
        'Dropper',
        'Lus',
        'Schaar',
        'Invoer stapelaar',
        'Doseer unit stapelaar']


@app.route('/')
def hello_world():
    get_db_items()
    return render_template('index.html', items=items, itemcount=len(items), menu=menu)


@app.route('/add', methods=["POST"])
def add_item():
    if request.method == "POST":
        data = {}
        data['time'] = request.json['time']
        data['error'] = request.json['error']
        print(data)
        items.append(Item(data['time'], data['error']))

        sql = "INSERT INTO storingen (tijd, reden) VALUES (%s, %s)"
        val = (data['time'], data['error'])
        mycursor.execute(sql, val)
        mydb.commit()

        return jsonify(data)


@app.route('/add-menu', methods=["POST"])
def add_dropdown():
    if request.method == "POST":
        data = {}
        data['menu'] = request.json['menu']
        print(data)
        menu.append(data['menu'])

        sql = "INSERT INTO menu (naam) VALUES (%s)"
        val = (data['menu'],)
        mycursor.execute(sql, val)
        mydb.commit()

        return jsonify(data)


@app.route('/update_item', methods=["POST"])
def update_item():
    if request.method == "POST":
        data = {}
        data['index'] = request.json['index']
        data['time'] = request.json['time']
        data['error'] = request.json['error']
        old_value = items[int(data['index'])].error
        items[int(data['index'])].error = data['error']

        sql = 'UPDATE storingen SET tijd = \'' + \
            data['time'] + '\', reden = \''+data['error'] + \
            '\' WHERE reden = \'' + old_value + \
            '\' AND tijd = \'' + data['time']+'\';'
        mycursor.execute(sql)
        mydb.commit()
        return jsonify(data)


def reset_data():
    global items, menu
    items = []
    menu = ['Vormwalsen',
            'Verkeerd geplaatse spacer',
            'Seperator storing',
            'Grijper I',
            'Grijper II',
            'Dropper',
            'Lus',
            'Schaar',
            'Invoer stapelaar',
            'Doseer unit stapelaar']


def get_db_items():
    reset_data()
    mycursor.execute(get_menu)
    myresult = mycursor.fetchall()
    print(myresult)
    for x in myresult:
        menu.append(x[1])

    mycursor.execute(get_storingen)
    myresult = mycursor.fetchall()
    print(myresult)
    for x in myresult:
        items.append(Item(x[1], x[2]))


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=False)
