var URLITEM = "http://0.0.0.0:80/add";
var URLUPDATE = "http://0.0.0.0:80/update_item";
var URLMENUITEM = "http://0.0.0.0:80/add-menu";
var HTTPMETHOD = "POST"

window.onload = function () {
    document.getElementById('btn').addEventListener("click", saveItem);
    document.getElementById('menu-btn').addEventListener("click", saveMenu);
}

function saveItem() {
    let currentDate = new Date();
    let date = currentDate.getDate() + "-" + (currentDate.getMonth() + 1) + "-" + currentDate.getFullYear();
    let time = currentDate.getHours() + ":" + currentDate.getMinutes() + ":" + currentDate.getSeconds();

    var timejson = date + ' ' + time;
    var menu = document.getElementById('storing');
    var error = menu.options[menu.selectedIndex].value;

    $.ajax({
        type: "POST",
        url: URLITEM,
        contentType: "application/json",
        data: JSON.stringify({ 'time': timejson, 'error': error }),
        dataType: "json",
        success: function (response) {
            console.log(response);
            window.location.reload();
        },
        error: function (err) {
            console.log(err);
        }
    });
};

function saveMenu() {
    var menu = document.getElementById('menu-optie').value;
    if (menu !== '') {
        $.ajax({
            type: "POST",
            url: URLMENUITEM,
            contentType: "application/json",
            data: JSON.stringify({ 'menu': menu }),
            dataType: "json",
            success: function (response) {
                console.log(response);
                window.location.reload();
            },
            error: function (err) {
                console.log(err);
            }
        });
    }else{
        alert('Normaal doen he!')
    }
};

function edit(index) {
    var temp = index.split("-");
    document.getElementsByClassName('save')[0].style.display = 'block';
    document.getElementById(index).disabled = true;
    document.getElementById('error-' + temp[1]).disabled = false;
}

function save(index) {
    var temp = index.split("-");
    document.getElementsByClassName('save')[0].style.display = 'none';
    document.getElementById(index).disabled = false;
    var menu = document.getElementById('error-' + temp[1])
    var error = menu.options[menu.selectedIndex].value;
    var time = document.getElementById('time-' + temp[1]).innerText

    $.ajax({
        type: "POST",
        url: URLUPDATE,
        contentType: "application/json",
        data: JSON.stringify({ 'index': temp[1], 'time': time, 'error': error }),
        dataType: "json",
        success: function (response) {
            console.log(response);
            window.location.reload();
        },
        error: function (err) {
            console.log(err);
        }
    });
}
